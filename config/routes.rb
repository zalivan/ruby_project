Rails.application.routes.draw do

  root to: 'phrases#index'

  devise_for :users, :controllers => { registrations: 'user/registrations' }

  resources :phrases

  resources :users, only: [:show, :index]

  resources :phrases do
    member do
      post :vote
    end
    resources :examples, only: [:create, :destroy] do
      post :vote
    end
  end

  resources :notifications, only: :index do
    collection do
      put :read_all
    end
  end

end
