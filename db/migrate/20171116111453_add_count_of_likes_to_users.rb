class AddCountOfLikesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :count_of_likes, :integer, default: 0
  end
end
