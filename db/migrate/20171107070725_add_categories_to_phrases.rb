class AddCategoriesToPhrases < ActiveRecord::Migration[5.1]
  def change
    add_column :phrases, :category, :integer, default: 0
  end
end
