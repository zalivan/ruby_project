class AddDesciptionToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :description, :string
    add_column :users, :years_old, :integer
    add_column :users, :is_student, :boolean
  end
end
