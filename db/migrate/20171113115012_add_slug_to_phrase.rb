class AddSlugToPhrase < ActiveRecord::Migration[5.1]
  def change
    add_column :phrases, :slug, :string
  end
end
