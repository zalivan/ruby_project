module SharedMethods

  extend ActiveSupport::Concern

  def is_author?(user)
    self.user == user
  end

  def set_carma(vote, current_user)
    current_user_carma_points = current_user.carma
    author_carma_points = self.user.carma
    author_count_of_likes = self.user.count_of_likes
    author = self.user

    if self.class.name == 'Example'
      author_point = vote == 'up' ? 2 : -1
    else
      author_point = vote == 'up' ? 4 : -2
    end

    likes_point = vote == 'up' ? 1 : 0

    author.update_attribute('carma', author_carma_points + author_point)
    author.update_attribute('count_of_likes', author_count_of_likes + likes_point)
    current_user.update_attribute('carma', current_user_carma_points + 1)

    if self.user.count_of_likes % 5 == 0 && self.user.count_of_likes != 0 && vote == 'up'
      @user = self.user
      UserMailer.send_message(user).deliver
    end

  end

end