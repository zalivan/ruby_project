class User < ApplicationRecord

  extend FriendlyId
  include PublicActivity::Model

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :validatable, :trackable

  acts_as_voter

  has_many :phrases
  has_many :examples

  validates :username, presence: true
  validates :username, uniqueness: true

  validates :description, presence: true
  validates :years_old, presence: true, numericality: { only_integer: true, greater_than: -1, less_than: 100}

  friendly_id :username, use: :slugged

  def has_new_notifications?
    PublicActivity::Activity.where(recipient_id: self.id, readed: false).any?
  end
end
