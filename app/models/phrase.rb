class Phrase < ApplicationRecord

  extend FriendlyId
  include SharedMethods
  include PublicActivity::Model

  CATEGORIES = [['Actions', 0], ['Time', 1], ['Productivity', 2], ['Apologies', 3], ['Common', 4]]

  belongs_to :user
  has_many :examples, dependent: :destroy
  accepts_nested_attributes_for :examples, allow_destroy: true
  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy

  validates :phrase, presence: true, uniqueness: true
  validates :translation, :phrase, :user_id, presence: true
  validates :category,
            inclusion: {
                in: %w(Actions Time Productivity Apologies Common),
                message: '%{value} is not a valid categoty'
            }

  enum category: %w(Actions Time Productivity Apologies Common)

  friendly_id :phrase, use: :slugged
  acts_as_votable

  def author?(user)
    self.user == user
  end
end
