class Example < ActiveRecord::Base

  include SharedMethods
  include PublicActivity::Model

  acts_as_votable

  belongs_to :phrase
  belongs_to :user

  validates :example, presence: true
  validates_uniqueness_of :example, scope: :phrase_id, :message=>'has already been used!'
end