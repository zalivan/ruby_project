class ApplicationMailer < ActionMailer::Base

  default from: ENV['GMAIL_USERNAME'],
          template_path: 'user_mailer'
  layout 'mailer'

end
