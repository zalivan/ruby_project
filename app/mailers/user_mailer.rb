class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usergenerate_mailer.mailer.subject
  #
  # def mailer
  #   @greeting = "Hi"
  #
  #   mail to: "to@example.org"
  # end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usergenerate_mailer.UserMailer.subject
  #

  def send_message(user)
    mail(to: user.email, subject: "You have #{user.count_of_likes} likes!")
  end

  # def UserMailer
  #   @greeting = "Hi"
  #
  #   mail to: "to@example.org"
  # end
end
