class NotificationsController < ApplicationController
  before_action :params_activity

  def index
    @notifications = params_activity
  end

  def read_all
    params_activity.update_all({readed: true})
    render :nothing => true
  end

  protected

  def params_activity
    PublicActivity::Activity.where(recipient_id: current_user.id)
  end

end
