class UsersController < ApplicationController

  def show
    @user = User.friendly.find(params[:id])
    @phrases = @user.phrases
  end

  def index
    @users = User.order(carma: :desc).paginate(:page => params[:page])
  end
end
