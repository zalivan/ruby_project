class User::RegistrationsController < Devise::RegistrationsController

  protected

  def sign_up_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation, :description, :years_old, :is_student )
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def account_update_params
    params.require(:user).permit(:username, :email, :description, :years_old, :is_student)
  end

end