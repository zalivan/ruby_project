class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  def shared_vote(instance)
    if params[:vote] == 'up'
      instance.liked_by current_user
    else
      instance.downvote_from current_user
    end
    if instance.vote_registered?
      instance.set_carma(params[:vote], current_user)
      message = params[:vote] == 'up' ? 'Liked your' : 'Disliked your'
      instance.create_activity key: message, owner: current_user, recipient: instance.user
      flash[:notice] = 'Thanks for your vote!'
    else
      flash[:danger] = 'You\'ve already voted that post!'
    end
    redirect_back(fallback_location: root_path)
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :description, :years_old, :is_student])
  end

  private

  def forbid_user_vote(instance)
    if  instance.user.id == current_user.id
      flash[:danger] = 'You can\'t vote for yourself! Oh, Little cheater !'
      redirect_back(fallback_location: root_path)
    else
      shared_vote(instance)
    end
  end

end
