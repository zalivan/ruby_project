# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready turbolinks:change turbolinks:load', ->
    if (window.location.pathname == '/notifications')
      $.ajax '/notifications/read_all',
        type: 'PUT'
        dataType: 'json'
        success: (jqXHR, textStatus, errorThrown) ->
          console.log("Update was success!!")