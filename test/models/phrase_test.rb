require 'test_helper'

class PhraseTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "should not save phrases without current user" do
    phrase = Phrase.new
    assert_not phrase.save
  end

end
