# Preview all emails at http://localhost:3000/rails/mailers/usergenerate_mailer
class UsergenerateMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/usergenerate_mailer/mailer
  def mailer
    UsergenerateMailer.mailer
  end

  # Preview this email at http://localhost:3000/rails/mailers/usergenerate_mailer/UserMailer
  def UserMailer
    UsergenerateMailer.UserMailer
  end

end
