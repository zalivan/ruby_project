require 'test_helper'

class UsergenerateMailerTest < ActionMailer::TestCase
  test "mailer" do
    mail = UsergenerateMailer.mailer
    assert_equal "Mailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "UserMailer" do
    mail = UsergenerateMailer.UserMailer
    assert_equal "Usermailer", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
